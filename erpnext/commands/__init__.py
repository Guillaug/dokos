# Copyright (c) 2015, Web Notes Technologies Pvt. Ltd. and Contributors
# MIT License. See license.txt

import click

def call_command(cmd, context):
	return click.Context(cmd, obj=context).forward(cmd)

commands = []
